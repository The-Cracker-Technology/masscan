make

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Make... PASS!"
else
  # houston we have a problem
  exit 1
fi

make install

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Make install... PASS!"
else
  # houston we have a problem
  exit 1
fi

install -m 755 doc/masscan.8 /usr/share/man/man8/

mandb
